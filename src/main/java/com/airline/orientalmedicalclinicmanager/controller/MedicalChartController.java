package com.airline.orientalmedicalclinicmanager.controller;

import com.airline.orientalmedicalclinicmanager.entity.Customer;
import com.airline.orientalmedicalclinicmanager.model.CustomerRegister;
import com.airline.orientalmedicalclinicmanager.model.MedicalChartRegister;
import com.airline.orientalmedicalclinicmanager.service.CustomerService;
import com.airline.orientalmedicalclinicmanager.service.MedicalChartService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/medical-chart")
public class MedicalChartController {
    private final MedicalChartService medicalChartService;
    private final CustomerService customerService;

    @PostMapping("/new/customer-id/customerId")
    public String setMedicalChart(@PathVariable long customerId, @RequestBody @Valid MedicalChartRegister register) {
        Customer customer = customerService.getData(customerId);
        medicalChartService.setMedicalChart(customer, register);

        return "ok";
    }
}
