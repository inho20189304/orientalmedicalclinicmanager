package com.airline.orientalmedicalclinicmanager.controller;

import com.airline.orientalmedicalclinicmanager.model.CustomerRegister;
import com.airline.orientalmedicalclinicmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;
    @PostMapping("/register")
    public String setCustomer(@RequestBody @Valid CustomerRegister register) {
        customerService.setCustomer(register);

        return "ok";
    }
}
