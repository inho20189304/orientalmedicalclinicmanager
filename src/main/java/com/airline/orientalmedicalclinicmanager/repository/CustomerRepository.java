package com.airline.orientalmedicalclinicmanager.repository;

import com.airline.orientalmedicalclinicmanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository  extends JpaRepository<Customer, Long> {
}
