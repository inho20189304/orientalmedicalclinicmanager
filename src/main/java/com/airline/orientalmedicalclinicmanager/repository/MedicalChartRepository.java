package com.airline.orientalmedicalclinicmanager.repository;

import com.airline.orientalmedicalclinicmanager.entity.MedicalChart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicalChartRepository extends JpaRepository<MedicalChart, Long> {
}
