package com.airline.orientalmedicalclinicmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrientalMedicalClinicManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrientalMedicalClinicManagerApplication.class, args);
    }

}
