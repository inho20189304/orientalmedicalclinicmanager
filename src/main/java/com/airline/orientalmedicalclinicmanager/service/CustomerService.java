package com.airline.orientalmedicalclinicmanager.service;

import com.airline.orientalmedicalclinicmanager.entity.Customer;
import com.airline.orientalmedicalclinicmanager.model.CustomerRegister;
import com.airline.orientalmedicalclinicmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRegister register) {
        Customer addData = new Customer();
        addData.setCustomerName(register.getCustomerName());
        addData.setUniqueNumber(register.getUniqueNumber());
        addData.setCustomerPhone(register.getCustomerPhone());
        addData.setAddress(register.getAddress());
        addData.setReason(register.getReason());
        addData.setFirstVisit(LocalDate.now());

        customerRepository.save(addData);

    }
    public Customer getData(long id) {
        return customerRepository.findById(id).orElseThrow();
    }
}
