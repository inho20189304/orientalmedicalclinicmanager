package com.airline.orientalmedicalclinicmanager.service;

import com.airline.orientalmedicalclinicmanager.entity.Customer;
import com.airline.orientalmedicalclinicmanager.entity.MedicalChart;
import com.airline.orientalmedicalclinicmanager.model.MedicalChartRegister;
import com.airline.orientalmedicalclinicmanager.repository.MedicalChartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class MedicalChartService {
    private final MedicalChartRepository medicalChartRepository;

    public void setMedicalChart(Customer customer, MedicalChartRegister medicalChartRegister) {
        MedicalChart addData = new MedicalChart();
        addData.setCustomer(customer);
        addData.setMedicalItem(medicalChartRegister.getMedicalItem());
        addData.setIsInsurance(medicalChartRegister.getIsInsurance());
        addData.setCost(medicalChartRegister.getIsInsurance() ?
                medicalChartRegister.getMedicalItem().getInsurance() : medicalChartRegister.getMedicalItem().getUninsured());
        addData.setStartClinic(LocalDateTime.now());
        addData.setCalculate(false);

        medicalChartRepository.save(addData);
    }
}
