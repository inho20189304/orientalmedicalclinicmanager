package com.airline.orientalmedicalclinicmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter


public class CustomerRegister {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 9, max = 14) // 9는 여권 (외국인), 14는 주민번호 (내국인)
    private String  uniqueNumber;
    @NotNull
    @Length(min = 13, max = 13)
    private String  customerPhone;
    @NotNull
    @Length(min = 5, max = 100)
    private String address;
    @NotNull
    @Length(min = 2)
    private String reason;
}
