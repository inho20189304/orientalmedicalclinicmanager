package com.airline.orientalmedicalclinicmanager.model;

import com.airline.orientalmedicalclinicmanager.enums.MedicalItem;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MedicalChartRegister {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;

    @NotNull
    private Boolean isInsurance;
}
