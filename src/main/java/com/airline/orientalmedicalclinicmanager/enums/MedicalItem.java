package com.airline.orientalmedicalclinicmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
@Getter
@AllArgsConstructor
public enum MedicalItem {
    CHUNA("추나요법", 100000, 130000),
    HANBANG("한방", 150000, 200000),
    ACUPUNCTURE("침", 20000, 30000),
    SAP("수액", 35000, 50000)
    ;
    private final String medicalName;
    private final double insurance;
    private final double uninsured;

}
