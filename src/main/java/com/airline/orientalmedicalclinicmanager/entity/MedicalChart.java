package com.airline.orientalmedicalclinicmanager.entity;

import com.airline.orientalmedicalclinicmanager.enums.MedicalItem;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class MedicalChart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerId", nullable = false)
    private Customer customer;
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MedicalItem medicalItem;
    @Column(nullable = false)
    private Boolean isInsurance;
    @Column(nullable = false)
    private Double cost;
    @Column(nullable = false)
    private Boolean calculate;
    @Column(nullable = false)
    private LocalDateTime startClinic;

}
