package com.airline.orientalmedicalclinicmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 15)
    private String  uniqueNumber;
    @Column(nullable = false, length = 20)
    private String  customerPhone;
    @Column(nullable = false, length = 100)
    private String address;
    @Column(nullable = false, columnDefinition = "TEXT")
    private String reason;
    @Column(nullable = false)
    private LocalDate firstVisit;
}
